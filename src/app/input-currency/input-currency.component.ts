import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-input-currency',
  templateUrl: './input-currency.component.html',
  styleUrls: ['./input-currency.component.css']
})
export class InputCurrencyComponent implements OnInit {

  amount ;
  selectedCurrencyIndex ;

  rateList = [
              {currency: 'MYR', rate: 3.04},
              {currency: 'AUD', rate: 1.07},
              {currency: 'CNY', rate: 5.15},
              {currency: 'USD', rate: 0.74}
            ];

  constructor() { }

  ngOnInit() {
  }

}
